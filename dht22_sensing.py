#!/usr/bin/python

# for the Adafruit_DHT library:
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# for the MySQL and PhP part I also took for reference:
# http://www.instructables.com/id/Raspberry-PI-and-DHT22-temperature-and-humidity-lo/
# https://github.com/jjpFin/DHT22-TemperatureLogger/blob/master/DHT22logger.py 
# Author: Janne Posio

import sys
import time
import datetime
import MySQLdb
import Adafruit_DHT


# Parse command line parameters.
sensor_args = { '11': Adafruit_DHT.DHT11,
                '22': Adafruit_DHT.DHT22,
                '2302': Adafruit_DHT.AM2302 }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
    sensor = sensor_args[sys.argv[1]]
    pin = sys.argv[2]
else:
    print('usage: sudo ./Adafruit_DHT.py [11|22|2302] GPIOpin#')
    print('example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO #4')
    sys.exit(1)

# grab a sensor reading, the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
currentTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print (time.strftime("%c"))
if humidity is not None and temperature is not None:
    print('Temperature = {0:0.1f}*  Humidity = {1:0.1f}%'.format(temperature, humidity))
else:
    print('Failed to get reading. Try again!')
    sys.exit(1)

# database connection
db = MySQLdb.connect("localhost", "logger", "password", "temperatures")
curs = db.cursor()

# insert query
query = "INSERT INTO temperaturedata SET dateandtime='%s', temperature='%s', humidity='%s'" % (currentTime, temperature, humidity) 
try:
    curs.execute(query)
    db.commit()
except:
    print "Error: the database is being rolled back"
    db.rollback()
    sys.exit(0)        

# close stdout and stderr before exiting the script
try:
    sys.stdout.close()
except:
    pass
try:
    sys.stderr.close()
except:
    pass
