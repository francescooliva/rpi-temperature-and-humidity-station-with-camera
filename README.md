## A little project for the UC Irvine IoT specialization capstone on Coursera.

https://www.youtube.com/watch?v=OG_WmlhB90A

## Sources and Bibliography:

https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/overview  
http://www.instructables.com/id/Raspberry-Pi-remote-webcam/  
http://www.instructables.com/id/Raspberry-Pi-Temperature-Humidity-Network-Monitor/  
http://www.instructables.com/id/Raspberry-PI-and-DHT22-temperature-and-humidity-lo/  
http://raspberrywebserver.com/sql-databases/using-mysql-on-a-raspberry-pi.html/  
http://www.noip.com/support/knowledgebase/install-ip-duc-onto-raspberry-pi/  
https://www.raspberrypi.org/documentation/remote-access/  
https://www.raspberrypi.org/documentation/linux/usage/cron.md  
https://www.raspberrypi.org/learning/lamp-web-server-with-wordpress/  
